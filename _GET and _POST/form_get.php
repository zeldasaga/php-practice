<!doctype html>
<html>
<!-- This is the display for the data in the _GET collection.
     Data in the _GET collection is stored in an associative array
     based on the 'name' attribute of the submitted form inputs.
     Data is submitted via URL parameters, so they show up in the
     address bar. Never send sensitive data via _GET -->
    <body>
        This is your _GET data for name <?php echo $_GET['name']; ?> <br>
        This is your _GET data for email <?php echo $_GET['email']; ?>
    </body>

</html>