<!doctype html>
<html>
<body>
<!-- This is the display for the data in the _POST collection.
     Data in the _POST collection is stored in an associative array
     based on the 'name' attribute of the submitted form inputs
     This data is sent via HTTP request, so unlike _GET, it is
     not visible to users.  -->
<p>This is your _GET data for name: <strong class="text-success"><?php echo $_POST['post_name']; ?></strong></p> <br>
<p>This is your _GET data for email:<strong class="text-success"> <?php echo $_POST['post_email']; ?></strong> </p>
</body>

</html>