<?php
 define("TITLE", "Bling Bling McBling");
?>

<!DOCTYPE html>
    <html>

    <head>
        <meta charset=""utf-8">
        <meta http-equiv="X-UA-Compatible" contents="IE-edge">
        <meta name="viewport" content=""width="device-width, initial-scale=1">
        <title><?php echo TITLE ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    </head>

    <body>
        <p>Here is some stuffs</p>
        <div class="container">
            <h1><?php echo TITLE; ?></h1>
            <a href="#"><?php echo TITLE ?></a>
        </div>
        <?php
            $username = "Fling Ding McDermont";
            echo ("Here is some more stuffs"). "<br>";
            echo ($username). "<br>";

        //norma array
        $userInfo = array(
            "Bingiton",
            "Johnny boy",
            35,
            42.021
        );

        //associative array
        //syntax is: custom key (instead of [0] or [1] etc) => value
        $people = array(
            "username" => "mdermont",
            "fullname" => "Mickey McDermont",
            "age" => 35
        );

        echo $people["username"] ."<br>";
        echo $people["fullname"] ."<br>";
        echo $people["age"]      ."<br>";

        //multi-demensional array
        $employees = array(
            array(
            "username" => "mdermont",
            "fullname" => "Mickey McDermont",
            "age" => 35
        ),
        array(
            "mollyd",
            "Molly McDermont",
            30
        )
        );

        echo "<hr><br>";
        echo $employees[0]["fullname"] ."<br>";
        echo $employees[1][1]          ."<br>";


        ?>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>

</html>
