<?php
    function rename_epically(){
        $first_name = $_POST["regular_name"];

        $epic_title = array (
            "the blood axe",
            "the conquerer",
            "the butcher of Normandy",
            "the annoying friend requester",
            "stronghammer",
            "Blank the unworthy",
            "firestorm",
            "the wild",
            "Master magus",
            "the porcelain beauty",
            "Master cook",
            "the savior of York",
            "warrior of light",
            "breaker of shields",
            "blade breaker",
            "ironforge",
            "bringer of rainbows"
        );

        return array($first_name, $epic_title);
    }

    function display_rename($first_name, $epic_title) {
        $rand_title = rand(0,sizeof($epic_title)-1);
        $change_name = strtoupper($first_name ." " .$epic_title[$rand_title]);
        echo "<hr style=\"border-color:#0000AA\">". $change_name;
    }
?>