
<?php
 //Define Constants here
 define("TITLE", "Give Your Name an Epic Title");
 include("functions.php");

 if(isset($_POST["title_submit"])){
     //call function
     //rename_epically();
 }
?>

<!DOCTYPE html>
    <html>

    <head>
        <meta charset=""utf-8">
        <meta http-equiv="X-UA-Compatible" contents="IE-edge">
        <meta name="viewport" content=""width="device-width, initial-scale=1">
        <title><?php echo TITLE ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    </head>

    <body>
    <?php



        ?>
        <div class="container">
            <h1><?php echo TITLE; ?></h1>
            <form class="col-sm-8 col-sm-offset-2" action="" method="post">
                <textarea placeholder="Type your name here" class="form-control input-lg" name="regular_name"></textarea><br>
                <button type="submit" class="btn btn-primary btn-lg pull-right" name="title_submit">Make Me Epic</button>
            </form>
        </div>
    <?php
        if(isset($_POST["title_submit"])){
            $name_val = rename_epically()[0];
            $title_val = rename_epically()[1];

            display_rename($name_val, $title_val);
        }
    ?>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>

</html>
